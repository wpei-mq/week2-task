package gui;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame implements Runnable {
    private class Canvas extends JPanel {
        public Canvas() {
            setPreferredSize(new Dimension(1280, 720));
        }
        @Override
        public void paint(Graphics g) {
            //get position of mouse pointer
            Point pos = getMousePosition();
            //grid parameters
            int xOffset = 10, yOffset = 10;
            int gridWidth = 35, gridHeight = 35;
            int numGridX = 20, numGridY = 20;

            Grid grid = new Grid(xOffset, yOffset, gridWidth, gridHeight, numGridX, numGridY, pos);
            grid.draw(g);
        }
    }

    private class Grid extends JPanel {
        private int xOffset, yOffset, gridWidth, gridHeight, numX, numY;
        private Point pos;
        public Grid(int xOffset, int yOffset, int gridWidth, int gridHeight, int numX, int numY, Point pos) {
            this.xOffset = xOffset;
            this.yOffset = yOffset;
            this.gridWidth = gridWidth;
            this.gridHeight = gridHeight;
            this.numX = numX;
            this.numY = numY;
            this.pos = pos;
        }
        public void draw(Graphics g) {
            //draw grid when called
            int[][] grid = new int[numX][numY];
            for(int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[i].length; j++) {
                    //set the top left corners of each rectangle
                    grid[i][0] = xOffset + (gridWidth * i);
                    grid[0][j] = yOffset + (gridHeight * j);

                    try {
                        //when the mouse is inside the grid, check to see which rectangle it's in, then color that one gray
                        if ((pos.x - 10) / 35.0 > (double)i && (pos.x - 10) / 35.0 < (double)i + 1 && (pos.y - 10) / 35.0 > (double)j && (pos.y - 10) / 35.0 < (double)j + 1) {
                            Cell cell = new Cell(grid[i][0], grid[0][j], gridWidth, gridHeight, Color.GRAY);
                            cell.fill(g);
                        } else {
                            Cell cell = new Cell(grid[i][0], grid[0][j], gridWidth, gridHeight, Color.BLACK);
                            cell.draw(g);
                        }
                    }
                    catch(NullPointerException pointerOutOfBounds) {
                        //when the mouse is outside the window, draw the standard grid
                        Cell cell = new Cell(grid[i][0], grid[0][j], gridWidth, gridHeight, Color.BLACK);
                        cell.draw(g);
                    }
                }
            }
        }
    }

    private class Cell extends JPanel {
        private int x, y, width, height;
        private Color c;
        public Cell(int x, int y, int width, int height, Color c) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.c = c;
        }
        public void fill(Graphics g) {
            //draws a filled rectangle
            g.setColor(c);
            g.fillRect(x, y, width, height);
        }
        public void draw(Graphics g) {
            //draws the outline of a rectangle
            g.setColor(c);
            g.drawRect(x, y, width, height);
        }
    }

    public static void main(String[] args) {
        MainWindow window = new MainWindow();
        window.run();
    }

    private MainWindow() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(new Canvas());
        this.pack();
        this.setVisible(true);
    }

    @Override
    public void run() {
        while(true) {
            this.repaint();
        }
    }
}
